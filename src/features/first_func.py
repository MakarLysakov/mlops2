import pandas as pd
import click

@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def first_func(input_path, output_path):
    df = pd.read_csv(input_path)

    df.to_csv(output_path)

if __name__ == "main":
    first_func("data/raw/fresh_data.csv", "data/external/fresh_data.csv")